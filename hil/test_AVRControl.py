import pytest
from hil.AVRControl import AVRControl

class TestAVRControl:
    @pytest.fixture(scope="class")
    def avr(self):
        """Setup the AVRControl instance for testing."""
        avr_device = AVRControl(port='/dev/ttyAMA2', baud_rate=9600, timeout=1)
        yield avr_device
        avr_device.close()

    def test_turn_main_led_on(self, avr):
        """Test turning on the main LED and check response."""
        response = avr.turn_main_led_on()
        assert response == "LED on PD2 is ON", f"Unexpected response: {response}"

    def test_check_led_on_status(self, avr):
        """Test checking the status of the main LED when it is supposed to be ON."""
        response = avr.check_led_status()
        assert response == "LED on PD2 is ON", f"Unexpected response: {response}"

    def test_turn_main_led_off(self, avr):
        """Test turning off the main LED and check response."""
        response = avr.turn_main_led_off()
        assert response == "LED on PD2 is OFF", f"Unexpected response: {response}"

    def test_check_led_off_status(self, avr):
        """Test checking the status of the main LED when it is supposed to be OFF."""
        response = avr.check_led_status()
        assert response == "LED on PD2 is OFF", f"Unexpected response: {response}"
