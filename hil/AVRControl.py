import serial
import time

class AVRControl:
    def __init__(self, port, baud_rate=9600, timeout=1):
        self.serial = serial.Serial(port, baud_rate, timeout=timeout)
        if not self.serial.isOpen():
            self.serial.open()

    def send_command(self, command):
        """
        Send a command to the AVR microcontroller.
        Command should be '1' to turn the main LED on, '0' to turn it off, or '?' to check the current state.
        """
        self.serial.write(command.encode())

    def read_response(self):
        """
        Read the response from the AVR microcontroller.
        Returns the response as a string.
        """
        time.sleep(0.1)  # Give some time for the response to be returned
        response = self.serial.read_all().decode().strip()
        return response

    def turn_main_led_on(self):
        """
        Turn the main LED on and read the response.
        """
        self.send_command('1')
        return self.read_response()

    def turn_main_led_off(self):
        """
        Turn the main LED off and read the response.
        """
        self.send_command('0')
        return self.read_response()

    def check_led_status(self):
        """
        Check the current status of the main LED and read the response.
        """
        self.send_command('?')
        return self.read_response()

    def close(self):
        """
        Close the serial connection.
        """
        self.serial.close()

# Example usage:
if __name__ == "__main__":
    import time
    
    avr = AVRControl(port='/dev/ttyAMA2', baud_rate=9600, timeout=1)
    # List of commands to execute
    commands = [
        avr.turn_main_led_on,
        avr.check_led_status,
        avr.turn_main_led_off,
        avr.check_led_status
    ]

    # Loop through the list of commands, execute each one, and print the result
    for command in commands:
        result = command()  # Call the command
        print(result)  # Print the result of the command
        time.sleep(1)
    avr.close()
