# Build environment to generate binaries
FROM debian as build
RUN apt-get update && apt-get install -y \
    make \
    gcc-avr \
    binutils-avr \
    avr-libc \
    avrdude \
 && rm -rf /var/lib/apt/lists/*

# Runtime environment to run unit tests
FROM debian as unittest
RUN apt-get update && apt-get install -y \
    make \
    gcc \
    libcmocka-dev \
    gcovr \
 && rm -rf /var/lib/apt/lists/*