#include <avr/io.h>
#include <util/delay.h>
#include "usart.h"

#define BAUD 9600
#define MYUBRR F_CPU/16/BAUD-1

// LED definitions
#define LED_PIN_MAIN PD2  // Main LED on PORTD, pin 2
#define LED_PIN_ON_STATE PD3  // Additional LED for determining loaded state on PORTD, pin 3
#define LED_PIN_ERROR PD4  // Error condition LED on PORTD, pin 4

int main(void) {
    USART_Init(MYUBRR);

    // Configure main LED as output
    DDRD |= (1 << LED_PIN_MAIN);  // Correct port for LED_PIN_MAIN
    PORTD &= ~(1 << LED_PIN_MAIN);  // Turn off main LED initially

    // Configure additional LED as output for loaded state indication
    DDRD |= (1 << LED_PIN_ON_STATE);
    PORTD &= ~(1 << LED_PIN_ON_STATE);  // Ensure additional LED is off initially

    // Configure error LED as output
    DDRD |= (1 << LED_PIN_ERROR);
    PORTD &= ~(1 << LED_PIN_ERROR);  // Ensure error LED is off initially

    // All initializations are done, turn on the LED_PIN_ON_STATE to indicate system is ready
    PORTD |= (1 << LED_PIN_ON_STATE);  // Turn on the loaded state LED

    char receivedByte;
    char ledState = 0;  // State of main LED

    while (1) {
        receivedByte = USART_Receive();

        switch (receivedByte) {
            case '1':
                PORTD |= (1 << LED_PIN_MAIN);  // Correct port for turning main LED on
                ledState = 1;
                USART_SendString("LED on PD2 is ON\n");
                break;
            case '0':
                PORTD &= ~(1 << LED_PIN_MAIN);  // Correct port for turning main LED off
                ledState = 0;
                USART_SendString("LED on PD2 is OFF\n");
                break;
            case '?':
                if (ledState == 1) {
                    USART_SendString("LED on PD2 is ON\n");
                } else {
                    USART_SendString("LED on PD2 is OFF\n");
                }
                break;
            default:
                // Toggle error LED on unexpected character
                PORTD ^= (1 << LED_PIN_ERROR);
                USART_Transmit(receivedByte);  // Echo the unexpected character
                break;
        }
    }
}
