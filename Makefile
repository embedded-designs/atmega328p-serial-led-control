# Define the compiler for AVR microcontroller projects
CC=avr-gcc
# Define the standard GCC compiler for non-AVR, general C tests
GCC=gcc

# Compiler flags for building with AVR-GCC; specify the microcontroller type and clock frequency
CFLAGS=-mmcu=atmega328p -DF_CPU=8000000UL -Os

# Compiler flags for non-AVR tests; includes debug information, disables optimizations, and enables coverage reports
TEST_FLAGS=-g -O0 --coverage

# Define the utility for object copying and setting output formats, specific to AVR
OBJCOPY=avr-objcopy

# Define the main target file name
TARGET=main

# Define the source directory
SRCDIR=src

# Define the test directory
TESTDIR=tests

# Primary build target: build the hex file from the ELF binary
all: build test load

# Build the hex file from the ELF binary
build: $(TARGET).hex

# Rule to create hex file from ELF file using AVR object copy utility
$(TARGET).hex: $(TARGET).elf
	$(OBJCOPY) -O ihex $< $@

# Rule to link object files into a single ELF file
$(TARGET).elf: $(SRCDIR)/main.o $(SRCDIR)/usart.o
	$(CC) $(CFLAGS) -o $@ $^

# Compile main.c into an object file
$(SRCDIR)/main.o: $(SRCDIR)/main.c $(SRCDIR)/usart.h
	$(CC) $(CFLAGS) -c -o $@ $(SRCDIR)/main.c

# Compile usart.c into an object file
$(SRCDIR)/usart.o: $(SRCDIR)/usart.c $(SRCDIR)/usart.h
	$(CC) $(CFLAGS) -c -o $@ $(SRCDIR)/usart.c

# Test target that compiles and runs the unit tests, then generates a coverage report
test: $(TESTDIR)/test_usart
	@echo "Running tests..."
	@CMOCKA_MESSAGE_OUTPUT=xml CMOCKA_XML_FILE=$(TESTDIR)/test_results.xml ./$(TESTDIR)/test_usart
	@gcovr -r .

# Compile the unit tests executable from test source files
$(TESTDIR)/test_usart: $(TESTDIR)/test_usart.c $(SRCDIR)/usart.c $(SRCDIR)/usart.h
	$(GCC) $(TEST_FLAGS) -I$(TESTDIR)/mocks -I$(SRCDIR) -o $@ $(TESTDIR)/test_usart.c $(SRCDIR)/usart.c -lcmocka

# Program the microcontroller using AVRDUDE
load:
	@echo "Programming the ATmega328P using Atmel-ICE..."
	avrdude -c atmelice_isp -p m328p -B 32 -U lfuse:w:0xe2:m
	avrdude -c atmelice_isp -p m328p -U flash:w:$(TARGET).hex:i

# Clean up all generated files
clean:
	rm -f $(SRCDIR)/*.o $(TARGET).elf $(TARGET).hex
	rm -f $(TESTDIR)/test_usart $(TESTDIR)/*.gcda $(TESTDIR)/*.gcno $(TESTDIR)/*.gcov coverage.xml

# Special targets that do not correspond to files
.PHONY: clean build test load all
