#ifndef _AVR_IO_H_
#define _AVR_IO_H_

#include <stdint.h>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-variable"

static uint8_t mock_UDR0 = 0;
static uint8_t mock_UBRR0H = 0;  // High byte of the baud rate
static uint8_t mock_UBRR0L = 103;  // Low byte of the baud rate, initialized for test

// Control Register A with bitfields
typedef union {
    uint8_t value;
    struct {
        uint8_t unused0 : 4; // Unused bits
        uint8_t udre0  : 1;  // USART data register empty
        uint8_t unused1 : 2; // More unused bits
        uint8_t rxc0   : 1;  // Receive complete
    } bits;
} UCSR0A_t;

static UCSR0A_t mock_UCSR0A = {0}; // Initialize to zero

static uint8_t mock_UCSR0B = 0;  // Control Register B
static uint8_t mock_UCSR0C = 0;  // Control Register C

#define UDR0    mock_UDR0
#define UBRR0H  mock_UBRR0H
#define UBRR0L  mock_UBRR0L
#define UCSR0A  mock_UCSR0A.value
#define UCSR0B  mock_UCSR0B
#define UCSR0C  mock_UCSR0C

#define RXEN0   4  // Example bit position
#define TXEN0   3  // Example bit position
#define UCSZ00  1  // Character size bit position
#define UDRE0   5  // Data register empty bit position
#define RXC0    7  // Receive complete bit position

#pragma GCC diagnostic pop

#endif
