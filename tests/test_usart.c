#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>
#include "usart.h"

static void test_USART_Init(void **state) {
    (void)state;  // Cast to void to suppress unused parameter warning
    USART_Init(9600);
    assert_int_equal(UBRR0L, 103);  // Assuming UBRR0L should be 103 for baud rate 9600 at F_CPU 16MHz
}

static void test_USART_Transmit(void **state) {
    (void)state; // Suppress unused parameter warning
    mock_UCSR0A.bits.udre0 = 1;  // Set the UDRE0 flag to simulate ready-to-transmit
    USART_Transmit('A');
    assert_int_equal(UDR0, 'A');
}

static void test_USART_Receive(void **state) {
    (void)state;  // Cast to void to suppress unused parameter warning
    mock_UDR0 = 'B'; // Set UDR0 to 'B'
    mock_UCSR0A.bits.rxc0 = 1;  // Set the RXC0 flag to simulate data ready to be read
    unsigned char received = USART_Receive();
    assert_int_equal(received, 'B');
    mock_UCSR0A.bits.rxc0 = 0;  // Clear the RXC0 flag after read
}

int main(void) {
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_USART_Init),
        // cmocka_unit_test(test_USART_Transmit),
        // cmocka_unit_test(test_USART_Receive),
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}
