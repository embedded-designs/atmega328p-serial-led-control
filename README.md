# ATmega328P Serial LED Control

## Overview
This project involves controlling an LED using serial commands on an ATmega328P microcontroller. It includes functionality to turn an LED on or off and query its current state via a serial interface.

## Compile Instructions

1. **Install AVR Toolchain**: Ensure the AVR toolchain (`avr-gcc`, `avr-libc`, `avr-binutils`, `avrdude`) is installed.

2. **Compile the Source Code**:
   ```
   avr-gcc -mmcu=atmega328p -DF_CPU=8000000UL -Os -o main.elf main.c
   avr-objcopy -O ihex main.elf main.hex
   ```

3. **Upload Using AVRDUDE**:
   ```
   avrdude -c atmelice_isp -p m328p -B 32 -U lfuse:w:0xe2:m
   avrdude -c atmelice_isp -p m328p -U flash:w:main.hex:i
   ```
   Note: We're setting the fuse bits first to use the 8 MHz onboard oscillator

## Docker Environment
We utilize multiple Docker targets to manage different phases of development:
- **Build Environment**: Setup for AVR compilation.
- **Unit Test Environment**: Setup for running C unit tests.
To build and run using Docker:
```
docker build -t avr-env --target build .
docker run -it avr-env
docker build -t unittest-env --target unittest .
docker run -it unittest-env
```

## Continuous Integration
Our project uses GitLab CI for automation, encompassing stages like build, test, and release. For local consistency with CI, use:
```
make test
make build
```
Refer to `.gitlab-ci.yml` for detailed CI configurations.

## Testing
Execute unit tests using:
```
make test
```
This command compiles and runs the tests, similar to the CI environment.

## Hardware in Loop (HIL) Testing
For HIL testing setup:
```
make load
pytest hil/test_AVRControl.py
```

## Wiring Setup
### USART and LED Connection
A Raspberry Pi is used as a UART device within the CI HIL configuration. The following is a sample hookup guide for serial communication
| Component       | ATmega328P Pin | USB-to-Serial Converter |
|-----------------|----------------|-------------------------|
| LED (anode)     | PB5 (Pin 19)   | -                       |
| LED (cathode)   | -              | GND                     |
| TX              | PD1 (Pin 3)    | RX                      |
| RX              | PD0 (Pin 2)    | TX                      |
| GND             | GND            | GND                     |

### ISP Programming Using ATMEL ICE
| Atmel-ICE AVR port pins     | ATmega328P Pin     |
|-----------------------------|--------------------|
| Pin 1 (TCK)                 | Pin 19 (PB5/SCK)   |
| Pin 2 (GND)                 | Pin 8  (GND)       |
| Pin 3 (TDO)                 | Pin 18 (PB4/MISO)  |
| Pin 4 (VTG)                 | Pin 7  (VCC)       |
| Pin 5 (TMS)                 |                    |
| Pin 6 (nSRST)               | Pin 1  (PC6/RESET) |
| Pin 7 (not connected)       |                    |
| Pin 8 (nTRST)               |                    |
| Pin 9 (TDI)                 | Pin 17 (PB4/MOSI)  |
| Pin 10 (GND)                |                    |

### Atmega328P Pinout
![ATmega328P Pinout Diagram](images/Atmega328P_Pinout.jpg)

## Power Considerations
Ensure that the ATmega328P and all components are powered correctly (typically with 5V). Verify that all parts share a common ground to prevent communication issues and potential damage.
